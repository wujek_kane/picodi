Przykłady mojego kodu
==============

Playlista
--------------

Tutaj praktycznie cały projekt (bez configow)
Prosta apka do zarządzania playlistą spotow reklamowych na tabletach
Nie ma tutaj żadnego uwierzytelniania, ponieważ to MVP i wystarczył basicAuth

Wtyczka do woocommerce - własna metoda dostawy
--------------
Dodatkowa metoda dostawy (dostawa na terenie Krakowa - ograniczona do kodow pocztowych za pomocą innej wtyczki),
darmowa dostawa powyżej określonej ilości sztuk.

Serwis aukcyjny - M.E.A.N. + Websockets
--------------
Projekt o ktorym wspominalem na dzisiejszej rozmowie.
Wybrałem najciekawsze fragmenty:

    -models/Auctions.js -> Schema aukcji w mongoose.js
    -bot/index.js -> Bot pobierający ekwipunek z konta Steam, przekazujący przedmioty zwyciezcom
    -sockets/index.js -> Kod odpowiedzialny za komunikację z użytkownikiem za pomocą Websockets

Serwis miał jeszcze rozbudowany panel administracyjny, bardzo dużo routingu, ale postanowiłem wybrac najciekawsze fragmenty.
Komentarze przetlumaczylem na j. polski, w wiekszosci przypadkow pisze je po angielsku.
