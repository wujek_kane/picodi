<?php
class Db
{
    public $pdo;
    public $url;

    public function __construct(){
        $this->connect();
    }

    private function connect()
    {
        $config = parse_ini_file('config.ini');
        $dsn = "mysql:host=".$config['host'].";dbname=".$config['dbname'].";charset=".$config['charset'];
        try {
            $this->pdo = new PDO($dsn, $config['username'], $config['password']);
            $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (PDOException $e) {
            die( 'Connection failed: ' . $e->getMessage());
        }

        $this->url = $config['url'];
    }
    /**
    *   Zapis nowego pliku w bazie po uploadzie
    **/
    public function insertFile( $name, $type, $md5 )
    {
        $url = $this->url.'/uploads/'.$name;
        $sql = "INSERT INTO files (name, url, type, md5) VALUES (:name, :url, :type, :md5)";
        $this->pdo->prepare($sql)->execute(['name' => $name, 'url' => $url, 'type' => $type, 'md5' => $md5]);
    }
    /**
    *  Pobranie wszystkich plikow z bazy
    **/
    public function getAllFiles()
    {
        $result = array();
        $sql = $this->pdo->query('SELECT * FROM files');

        $sql->execute();
        $results=$sql->fetchAll(PDO::FETCH_ASSOC);
        $result=json_encode($results);

        return $result;
    }
    /**
    *   Zapis playlisty w bazie (JSON)
    **/
    public function savePlaylist( $playlist )
    {
        $sql = "INSERT INTO playlist (playlist) VALUES (:playlist)";
        if ($this->pdo->prepare($sql)->execute( ['playlist' => $playlist] )) {
            return true;
        } else {
            return false;
        }
    }
    /**
    *   Pobranie playlisty z bazy (JSON)
    *   Jest tylko jedna, dlatego ostatnia, nie konkretne ID
    **/
    public function getPlaylist()
    {
        $result = array();
        $sql = $this->pdo->query('SELECT playlist FROM playlist ORDER BY `ID` DESC LIMIT 0,1');

        $sql->execute();
        $results=$sql->fetchAll(PDO::FETCH_ASSOC);
        $result=json_encode($results["playlist"]);

        return $results;
    }
    /**
    *   Usuniecie pliku z bazy
    **/
    public function deleteFile( $id )
    {
        $sql = $this->pdo->prepare('SELECT name FROM files WHERE `ID` = ? LIMIT 0,1');
        $sql->execute([$id]);
        $file=$sql->fetchAll(PDO::FETCH_ASSOC);

        $stmt = $this->pdo->prepare("DELETE FROM files WHERE ID = ?");
        $stmt->execute([$id]);
        $deleted = $stmt->rowCount();
        if ($deleted > 0) {
            return $file[0]['name'];
        } else {
            return false;
        }
    }
    /**
    *   Logowanie zgloszenia z tabletu
    *   Wersja SDK, numer seryjny, model, data
    *   Np: 5.0.1, I7HQO7L7VGOFI7W4, Lenovo TAB 2 A10-70L, 2017-03-28 22:25:17 
    **/
    public function log( $device_sdk, $device_serial, $device_model )
    {
        $sql = "INSERT INTO logs ( device_sdk, device_serial, device_model ) VALUES (:device_sdk, :device_serial, :device_model)";
        if ($this->pdo->prepare($sql)->execute( ['device_sdk' => $device_sdk, 'device_serial' => $device_serial, 'device_model' => $device_model] )) {
            return true;
        } else {
            return false;
        }

    }
}
