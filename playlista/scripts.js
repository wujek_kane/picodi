var playlist = {
    init: function( settings ) {
      $( '#playlist tbody' ).sortable();
      $( '#sortable' ).sortable();

      playlist.getFiles();
      playlist.getPlaylist();

    },
    getFiles: function() {
      var rows = new Array();
      $( '#files tbody' ).empty();
      $.getJSON( 'index.php?action=getAllFiles', function( data ) {
        var parsed = $.parseJSON( data );
        $.each( parsed, function( key, value ) {
          var md5 = value.md5 || 0;
          rows.push( '<tr><td><a href="#" data-id="'+value.ID+'" data-name="'+value.name+'" data-url="'+value.url+'" data-type="'+value.type+'" data-md5='+md5+' class="add ui-button btn btn-primary">  <span class="glyphicon glyphicon-triangle-left" aria-hidden="true"></span></a></td><td>' + value.ID +'</td><td>' + value.name +'</td><td><button class="deleteFile btn btn-danger" data-id="'+value.ID+'" data-name="'+value.name+'"><span class="glyphicon glyphicon-remove-sign" aria-hidden="true"></span></button></td></td></tr>' );

        });
        if ($( '#files' ).append( rows )){
          playlist.bindActions();
        };

      });
    },
    getPlaylist: function() {
      var rows = new Array();
      $( '#playlist tbody' ).empty();
      $.getJSON( 'index.php?action=getPlaylist', function( data ) {
        var parsed = $.parseJSON( data[0]['playlist'] );
        $.each( parsed, function( key, value ) {
          var md5 = value.md5 || 0;
          rows.push( '<tr data-id="'+value.id+'" data-name="'+value.name+'" data-url="'+value.url+'" data-type="'+value.type+'" data-md5="'+md5+'"><td><span class="glyphicon glyphicon-move" aria-hidden="true"></span>'+value.name+' <button class="delete btn btn-danger">usuń</button></td></tr>' );
        });

        if ($( '#playlist' ).append( rows )){
          playlist.bindActions();
        };

      });

    },

    bindActions: function(){
      console.log('BIND ACTIONS' );
      $( ".save" ).unbind();
      $( ".add" ).unbind();
      $( ".delete" ).unbind();
      $( ".deleteFile" ).unbind();
      $( ".reset" ).unbind();
      $( ".debug" ).unbind();

      $( ".debug" ).on( 'click', playlist.debug );
      $( ".add" ).on( 'click', playlist.addToPlaylist );
      $( '.delete' ).on( 'click', playlist.deleteFromPlaylist );
      $( '.save' ).on( 'click', playlist.savePlaylist );
      $( '.reset' ).on( 'click', playlist.getPlaylist );
      $( '.deleteFile' ).on( 'click', playlist.deleteFile );
    },

    addToPlaylist: function(){
      console.log( 'clicky' );
      var zmienna = $(this).data('id') + ' | ' + $(this).data('name');
      var id = $(this).data('id');
      var name = $(this).data('name');
      var url = $(this).data('url');
      var type = $(this).data('type');
      var time = $(this).data('time');
      var md5 = $(this).data('md5') || "";
      var row = '<tr data-id="'+id+'" data-name="'+name+'" data-url="'+url+'" data-type="'+type+'" data-md5="'+md5+'"><td><span class="glyphicon glyphicon-move" aria-hidden="true"></span>'+name+' <button class="delete btn btn-danger">usuń</button></td></tr>';

      if ($( '#playlist' ).append( row )) {
        playlist.bindActions();
      };

    },

    deleteFromPlaylist: function(){
      $(this).parents('tr').remove();
    },

    savePlaylist: function(){
      var items = [];
      var object = {};

      $( '#playlist tr' ).each(function() {
        var object = {};
        object.id = $(this).data('id');
        object.name = $(this).data('name');
        object.url = $(this).data('url');
        object.type = $(this).data('type');
        object.md5 = $(this).data('md5');
        items.push( object );
      });

      var json = JSON.stringify( items );

      $.ajax({
        url: 'index.php?action=savePlaylist',
        type: "POST",
        data: {playlist: json},
        complete: function( response ){
          console.log( response.status );
          if ( response.status === 200) {
            console.log( 'ok' );
            playlist.alert( 'success', 'Zapisano pomyślnie' );
          } else {
            playlist.alert( 'danger', 'Błąd zapisu' );
          }
        }
    });

  },
  deleteFile: function(){
    var id = $(this).data('id');
    var name = $(this).data('name');
    var r = confirm("Czy na pewno usunąć plik "+ name+"?");
    if (r) {
      $.ajax({
        url: 'index.php?action=deleteFile',
        type: "POST",
        data: {id: id},
        complete: function( response ){
          console.log( response.responseText );
          if ( response.status === 200) {
            console.log( 'ok' );
            playlist.alert( 'warning', 'Usunięto plik' );
            playlist.getFiles();
          } else {
            playlist.alert( 'danger', 'Błąd usuwania pliku' );
          }
        }
    });
      //console.log('deleted');
    } else{
      //console.log('nothing');
    }
  },
    alert: function( type, message ){
      var alert = $('<div class="alert alert-' + type + '"><a href="#" class="close" data-dismiss="alert">&times;</a>' + message + '</div>');
      $( '.alerts' ).append( alert );
      setTimeout(function(){
        alert.fadeOut(500);
      }, 2000);
    },
    debug: function (){
      console.log( 'debug' );
      playlist.bindActions();
    }

    }
$( document ).ready( playlist.init );
