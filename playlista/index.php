<?php
include('inc/db.php');

$db = new Db();
$action = $_GET['action'];

switch ( $action ) {
    case 'getAllFiles':
        echo json_encode( $db->getAllFiles() );
        break;
    case 'savePlaylist':
        $result = $db->savePlaylist( $_POST['playlist'] );
        if ( $result === false ){
            http_response_code( 400 );
        }
        break;
    case 'getPlaylist':
        echo json_encode( $db->getPlaylist() );
        break;
    case 'deleteFile':
        $file_name = $db->deleteFile( $_POST['id']);
        if ( $file_name !== false ){
            print_r ( $file_name );
            if ( unlink('uploads/'.$file_name )){
              return true;
            } else {
              http_response_code( 400 );
              return false;
            }
        }
        break;
    /* LOGOWANIE ZAPYTAŃ Z TABLETOW */    
    default:
        $info = $_GET["info"];
        if ( !empty($info) ){
            $info = urldecode( $info );
            $device = explode(' | ', $info);
            $device_sdk = str_replace('Device SDK: ', '', $device[0] );
            $device_serial = str_replace('Serial: ', '', $device[1] );
            $device_model = str_replace('Model: ', '', $device[2] );
            $db->log( $device_sdk, $device_serial, $device_model );
        }
        $result = $db->getPlaylist();
        echo '{"playlist": '.$result[0]["playlist"].'}';
        break;
}

 ?>
