<!DOCTYPE html>
<html lang="pl">
  <head>
    <meta charset="utf-8">
<head>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
</head>
<body>
  <nav class="navbar navbar-default">
    <div class="container-fluid">
      <!-- Brand and toggle get grouped for better mobile display -->
      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="#">Wawel LM</a>
      </div>

      <!-- Collect the nav links, forms, and other content for toggling -->
      <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
        <ul class="nav navbar-nav">
          <li><a href="index.html">Home</a></li>
          <li class="active"><a href="upload.html"><span class="sr-only">(current)</span>Upload</a></li>

        </ul>

        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
  </nav>
  <div class="container">
    <div class="row">



<?php
require('inc/db.php');
$db = new Db();

$target_dir = "uploads/";
$hash = generateHash();
$target_name = basename($_FILES["fileToUpload"]["name"]);
$target_name = explode('.', $target_name);
$target_name = $target_name[0].'_'.$hash.'.'.$target_name[1];
$target_file = $target_dir . $target_name;
$uploadOk = 1;
$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);

if( isset($_POST["submit"]) ){
    $mime = mime_content_type($_FILES["fileToUpload"]["tmp_name"]);
    $file_type = explode('/', $mime);
    $file_type = $file_type[0];

if ( ($file_type != 'video') && ($file_type != 'image') ){
    echo "Plik nie jest filmem ani obrazem";
    $uploadOk = 0;
} else {
    $uploadOk = 1;
}

} else { //debug
  print_r($db->getAllFiles());
}
// Check if file already exists
if (file_exists($target_file)) {
    echo "Sorry, file already exists.";
    $uploadOk = 0;
}
// Check file size
if ($_FILES["fileToUpload"]["size"] > 10000500000) {
    echo "Sorry, your file is too large.";
    $uploadOk = 0;
}
// Check if $uploadOk is set to 0 by an error
if ($uploadOk == 0) {
    echo "Sorry, your file was not uploaded.";
// if everything is ok, try to upload file
} else {
    if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
        echo "<h2>Plik ". $target_name. " został przesłany.</h2><br />";
        echo '<a class="btn btn-primary" href="upload.html"><span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span>Powrót</a>';
        //$result = $db -> query("INSERT INTO `files` (`ID`,`name`) VALUES (""," . $target_name . ")");
        $md5 = md5_file( $target_file );
        //echo $md5;
        $db->insertFile( $target_name, $file_type, $md5 );
        /* insert */

    } else {
        echo "Wystąpił błąd przy przesyłaniu pliku.";
    }
}
function generateHash( $len = 5 ){
  $seed = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
$hash = sha1(uniqid($seed . mt_rand(), true));

# To get a shorter version of the hash, just use substr
$hash = substr($hash, 0, $len);
return $hash;
}

?>
</div>
</div>

</body>
