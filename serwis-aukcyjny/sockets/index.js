var server = require('./../app.js'),
    io = require('socket.io')(server),
    Auction = require('./../models/Auctions.js');
    User = require('./../models/Users.js');
var auction = new Auction();
var usersOnline = [];
var Sockets = {};
io.on('connect', function(socket){
    var counter = 0;
    var user;
    var token_;
    console.log('User connected: '+socket.id);
    socket.emit('init', 'You are connected');
    auction.getAll(function (err, auctions){
        console.log(err);
        socket.emit('auctions', auctions);
    });

    socket.on('disconnect', function(){
       console.log('User disconnected');
       usersOnline.forEach(function(entry, index){
         console.log( entry );
         if ( entry.socket_id == socket.id ){
           usersOnline.splice( index, 1);
         }
       })
    });

  socket.on('getAuctions', function () {
    auction = new Auction();
    auction.getAll(function (err, auctions){
      console.log(err);
      socket.emit('auctions', auctions);
    });
  })
  socket.on('bid', newBid);

  socket.on('token', function(token) { //ONCE IS IMPORTANT!
    console.log('got token');
    token_ = token;
    User.findOne({token: token}, function(err, user){
      if ( user ) {
        console.log( user );
        console.log('Token OK');
        var already = false;
        usersOnline.forEach(function(entry, index){

          if ( entry._id == user._id ){
          console.log('ech');
            already = true;
            //usersOnline.splice( index, 1);
          }
        })

        if (already == false) {
          var user = {
            _id:        user._id.toString(),
            socket_id:  socket.id,
            steamName:  user.steamName
          }
          usersOnline.push( user );
        }
        socket.emit('yourBids', user.bids);

      }
    })
  });


  socket.on('message', function(message){
    console.log(message);
  });

  function newBid( bid ){
    console.log('NEW BID');
    console.log( token_ );
    console.log('bid');
    user = bid.user;


    User.findOne({token: user.token}, function(err, user){
      if ( user.bids <= 0 ) {
        socket.emit('message', 'Nie masz podbić');
        return 0;
      }

  //auction = new Auction();
    Auction.findOne({_id: bid.auctionID, status: 'active', time_left: {$gt: 0}}, function(err, auction){
      if ( auction ) {
      if (auction.time_left < 90){
        auction.time_left += 3;
      }
      auction.bids += 1;
      auction.winner = user.steamName;
      auction.winner_id = user._id;
      var bid = {bidder: user.steamName, bidder_id: user._id};
      auction.bids_.addToSet(bid) ;

      auction.save();
      user.bids-=1;
      user.save();

      socket.emit('message', 'BID Successful');
      socket.emit('yourBids', user.bids);
      io.emit('newBid', { time_left: auction.time_left, _id: auction._id, winner: auction.winner});
    } else {
      console.log( 'Bid arrived after auction finished');
    }
    });

        });
    }
});


function ensureAuthenticated(req, res, next) {
  if (req.isAuthenticated()) { return next(); }
  res.redirect('/');
}

Sockets.init = function(){
    setInterval(() => {
        console.log('set interval');
        auction.start(function(res){
            if (res) {
                io.emit('newAuction', res);
                console.log( res );
            }
        });
        //var auction = new Auction();
        auction.tick(function(res){

            //SOMETHING ENDED
            if ( res === true ) {
                console.log('auction ended');
            } else {
            auction.getAll(function (err, auctions){
            auctions.forEach( function(auction) {
            console.log( 'update auction ' + auction._id + ' time left: ' + auction.time_left );
            io.emit('updateAuction', auction);
            });

            });
            }

        });
    }, 1000);
}

Sockets.usersOnline = function(){
  return usersOnline;
}

module.exports = Sockets;
