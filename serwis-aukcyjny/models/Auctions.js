var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var Counter = require('../models/Counters.js');

var bidSchema = new Schema({
    bidder: String,
    bidder_id: String,
    date: { type: Date, default: Date.now },
});

// Tworzymy scheme
var auctionSchema = new Schema({
    title: String,
    image: String,
    location: String,
    assetid: String,
    starts_at: Date,
    created_at: Date,
    updated_at: Date,
    ended_at: Date,
    winner: String,
    created_by: String,
    winner_id: String,
    bids: {
        type: Number,
        'default': 0
    },
    bids_: [
    {
        bidder: String,
        bidder_id: String,
        date: {
        type: Date,
        default: Date.now}
    }],
    time_left: {
        type: Number,
        'default': 2700 // Domyślnie 45min (2700s)
    },
    status: {
        type: String,
        'default': 'pending' // Domyślnie nadchodząca
    },
    sort: Number,
    show_pending: {
        type: Boolean,
        'default': false // Czy ma się pokazywac w nadchodzacych (domyslnie nie)
    },
    claimed: {
        type: Boolean,
        'default': false // Czy przedmiot zostal juz odebrany
    },
    },
        {timestamps: {  } }
    );


auctionSchema.methods.insertNew = function(data, callback) {
    console.log(data);

    auction = new Auction();
    auction._id = data.classid+'_'+data.instanceid;
    auction.title = data.market_name;
    auction.image = 'https://steamcommunity-a.akamaihd.net/economy/image/'+data.icon_url_large;
    auction.starts_at = data.mytime;
    auction.created_by = req.user.username;
    console.log(auction);
    auction.save(function(err, resp){


    return callback(err, resp);
});
}

auctionSchema.methods.getAll = function(callback) {
    console.log('auction get all');
        Auction.find({ status: { $in: [ 'active', 'finished' ] } }).limit(8).sort({status: 1, sort: -1}).lean().exec(function (err, result) {
        return callback(null, result);

    });
},

auctionSchema.methods.start = function(callback){
    console.log( 'auction start' );
    var now = new Date().toISOString();
    console.log( now );
    var count = new Counter();

    Auction.count({status: 'active'}, function(err, c) {

           if ( c < 8 ){ //jesli mamy mniej niz 8 aktywnych aukcji
               count.getNextSequence('auction', function(sequence){
                   Auction.findOneAndUpdate( {starts_at: { $lt: now }, status: "pending"}, {$set: {status: "active", sort: sequence}}, {upsert: false, multi: false}, function(err, res){
                       if ( res ){
                           count.updateSequence();
                       }
                    return callback(res);
                });
              })
           } else {
             console.log('else');
           }

       });
},

auctionSchema.methods.tick = function(callback){

  var now = new Date();
  Auction.update( { status: "active" }, {$inc: {time_left: -1}}, { upsert: false, multi: true } , function(err, res){
      //callback(res);
  });

  Auction.update({time_left: {$lt: 0}, status: "active"}, {status: "finished", ended_at: now}, { upsert: false, multi: true }, function(err, res){
    //console.log('ending:'+res);
    if ( res.nModified > 0 ){
      return callback (true);
    } else {
      return callback(false);
    }
  });
},

auctionSchema.methods.getOne = function(auctionID, callback) {
    Auction.findOne({_id: auctionID}, function (err, result) {
        if (err) return handleError(err);
            callback(null, result);
    })
},

auctionSchema.methods.update = function(id, data, callback) {
  console.log('update::::');
  console.log('data: '+ data);
  console.log('id: '+ id);
  Auction.findByIdAndUpdate(id, data, function(err, o){
    if (err) return handleError(err);
    console.log(o);
  });
},

auctionSchema.methods.checkStatusByAssetId = function ( assetid ) {
  Auction.findOne({assetid: assetid}, function (err, result) {
  if (err) return handleError(err);
  return result;
});

}

function getNextSequence(name) {
    var ret = Counter.findAndModify(
          {
            query: { _id: name },
            update: { $inc: { seq: 1 } },
            new: true
          }
    );
    return ret.seq;
}

var Auction = mongoose.model('Auction', auctionSchema);

module.exports = Auction;
