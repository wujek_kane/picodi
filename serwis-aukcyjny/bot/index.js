/**
	* To jest bot, sluzacy do przekazywania przedmiotow na platformie Steam
 */

var SteamUser = require('steam-user');
var Steamcommunity = require('steamcommunity');
var SteamTotp = require('steam-totp');
var TradeOfferManager = require('steam-tradeoffer-manager');
var fs = require('fs');
var config = require('./../config')();

var client = new SteamUser({
	'dataDirectory': '/srv/gamebid/bot'
});
var manager = new TradeOfferManager({
	"steam": client,
	"domain": config.realm,
	"language": "pl" // Polskie opisy przedmiotow
});
var community = new Steamcommunity();

var authCode = config.steam.authCode;

// Logowanie do Steam
var logOnOptions = {
	"accountName": config.steam.accountName,
	"password":config.steam.password,
	"twoFactorCode": SteamTotp.getAuthCode(config.steam.sharedSecret)

};

var confirmation_key = SteamTotp.getConfirmationKey(config.steam.identitySecret, config.steam.time, 'conf');
	console.log(confirmation_key);

try {
  logOnOptions.sha_sentryfile = getSHA1(fs.readFileSync('sentry'));
	console.log(e);
} catch (e) {
  if (authCode !== '') {
    logOnOptions.auth_code = authCode;
  }
}


client.on('loggedOn', function() {
	console.log("Logged into Steam");
});

client.on('webSession', function(sessionID, cookies) {

	manager.setCookies(cookies, function(err) {
		if (err) {
			console.log(err);
			console.log('fail');

		}

		console.log("Got API key: " + manager.apiKey);

	});

	community.setCookies(cookies);
	console.log('SET COOKIES');
	community.startConfirmationChecker(30000, config.steam.identitySecret); // Sprawdza i akceptuje potwierdzenia co 30s
});

manager.on('newOffer', function(offer) {
	console.log("New offer #" + offer.id + " from " + offer.partner.getSteam3RenderedID());
	offer.accept(function(err) {
		if (err) {
			console.log("Unable to accept offer: " + err.message);
		} else {
			community.checkConfirmations(); // Sprawdza potwierdzenia zaraz po zaakceptowaniu oferty
			console.log("Offer accepted");
		}
	});
});

manager.on('receivedOfferChanged', function(offer, oldState) {
	console.log("Offer #" + offer.id + " changed: " + TradeOfferManager.getStateName(oldState) + " -> " + TradeOfferManager.getStateName(offer.state));

	if (offer.state == TradeOfferManager.ETradeOfferState.Accepted) {
		offer.getReceivedItems(function(err, items) {
			if (err) {
				console.log("Couldn't get received items: " + err);
			} else {
				var names = items.map(function(item) {
					return item.name;
				});

				console.log("Received: " + names.join(', '));
			}
		});
	}
});

manager.on('pollData', function(pollData) {
	fs.writeFile('polldata.json', JSON.stringify(pollData));
});

community.on('sessionExpired', function(err) {
	if (err) {
		console.log('sessionExpired: '+err);
	}

	community.stopConfirmationChecker();

	if (client.steamID) {
		client.webLogOn();
		console.log('==============client.webLogOn');
	} else {
		console.log('==============client.logOn ==== NEW AUTH CODE');
		/* Świeży kod */
		var logOnOptions = {
			"accountName": config.steam.accountName,
			"password":config.steam.password,
			"twoFactorCode": SteamTotp.getAuthCode(config.steam.sharedSecret)
				};
		client.logOn(logOnOptions);
	}
});

/*	Generujemy kod autentykacji SteamTotp
*	To jest emulacja autentykacji dwuskladnikowej na urzadzeniach mobilnych
*/
manager.getAuthCode = function () {
	 var code = SteamTotp.getAuthCode(config.steam.sharedSecret);
	 return code;
}

var bot = {
	/* Ładowanie ekwipunku z konta naszego bota */
	loadInventory: function ( callback ) {
	console.log( 'load inventory' )	;
	manager.loadInventory(730, 2, true, function(err, items){
		if ( err ) {
			console.log( err );
			client.webLogOn();
			setTimeout (bot.loadInventory( callback ), 5000);
		}
		if ( items ) {
		//console.log( items );
		callback( items );
		}
		//return items;
	});
	},
	/* Wysyłanie oferty (po tym jak user kliknie, że chce odebrac przedmiot) */
	sendOffer: function ( steam_id, assetid, token, callback ) {
		var offer = manager.createOffer(steam_id);
			offer.addMyItem({appid: 730, contextid: 2, assetid: assetid});
			offer.send('message', token, function(err, status){

			if ( err ) {
				console.log( err );
				client.webLogOn();
				//setTimeout (bot.sendOffer( steam_id, assetid, token, callback ), 5000);
				callback( 'error' );
			} else {
				callback( null );
			}

	});

},
getAuthCode: function () {
	 var code = SteamTotp.getAuthCode(config.steam.sharedSecret);
	 return code;
}

}

module.exports = bot;
