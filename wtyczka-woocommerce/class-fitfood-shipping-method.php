<?php
class WC_Fitfood_Shipping_Method extends WC_Shipping_Method
{
    public function __construct( $instance_id = 0 )
    {
        $this->id = 'fitfood_shipping_method';
        $this->instance_id = absint( $instance_id );
        $this->method_title = __( 'Dowóz na terenie Krakowa', 'woocommerce' );

        // Ladowanie ustawien
        $this->init_form_fields();
        $this->init_settings();

        // Ustawienia
        $this->enabled	= $this->get_option( 'enabled' );
        $this->title 	= $this->get_option( 'title' );

        $this->supports  = array(
        'shipping-zones',
        'instance-settings',
        'instance-settings-modal',
        );


        add_action( 'woocommerce_update_options_shipping_' . $this->id, array( $this, 'process_admin_options' ) );
    }

    public function init_form_fields()
    {
        $this->form_fields = array(
        'enabled' => array(
                            'title'		=> __( 'Enable/Disable', 'woocommerce' ),
                            'type' 		=> 'checkbox',
                            'label'		=> __( 'Włącz dowóz na terenie Krakowa', 'woocommerce' ),
                            'default'	=> 'yes'
        ),
        'title' => array(
                            'title' 		=> __( 'Nazwa metody dostawy', 'woocommerce' ),
                            'type' 			=> 'text',
                            'description' 	=> __( 'Nazwa ktora user widzi w podsumowaniu zamowienia', 'woocommerce' ),
                            'default'		=> __( 'Dostawa na terenie Krakowa', 'woocommerce' ),
        )
        );
    }

    public function is_available( $package )
    {
        foreach ( $package['contents'] as $item_id => $values ) {
            $_product = $values['data'];
            $weight =	$_product->get_weight();
            if( $weight > 10 ){
                return false;
            }
        }

        return true;
    }

    public function calculate_shipping( $package = array() )
    {
        $number_of_items = count ( $package );
        $cost = 10;
        $weight = 0;
        $dimensions = 0;
        foreach ( $package['contents'] as $item_id => $values ) {
            $_product  = $values['data'];
            $weight =	$weight + $_product->get_weight() * $values['quantity'];
            $dimensions = $dimensions + (($_product->length * $values['quantity']) * $_product->width * $_product->height);
        }

        // Koncowy koszt dla usera
        $this->add_rate( array(
            'id' 	=> $this->id,
            'label' => $this->title,
            'cost' 	=> $cost
        ));
        }
}
