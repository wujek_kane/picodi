<?php
/*
Plugin Name: FitFOOD Shipping
Plugin URI: http://cane-studio.pl
Description: FitFOOD Shipping
Version: 1.0.0
Author: cs
Author URI: http://cane-studio.pl
*/

$active_plugins = apply_filters( 'active_plugins', get_option( 'active_plugins' ) );
if ( in_array( 'woocommerce/woocommerce.php', $active_plugins) ) {
    add_filter( 'woocommerce_shipping_methods', 'add_fitfood_shipping_method' );
    function add_fitfood_shipping_method( $methods )
    {
        $methods['fitfood_shipping_method'] = 'WC_Fitfood_Shipping_Method';
        return $methods;
    }

    add_action( 'woocommerce_shipping_init', 'fitfood_shipping_method_init' );
    function fitfood_shipping_method_init()
    {
        require_once 'class-fitfood-shipping-method.php';
    }
}
